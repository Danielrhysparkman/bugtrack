﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bugtrack
{
    public partial class Form5 : Form
    {
        // Initilise variables
        private SqlConnection mySqlConnection;

        public Form5()
        {
            //Entry Point
            InitializeComponent();

            //Sql Connection
            //mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=I:\Computing\Third_Year\ASEA\Bugtrack\Bugtrack.mdf;Integrated Security=True;Connect Timeout=30");
            mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=E:\Computing\Third_Year\ASEA\Bugtrack\Bugtrack\Bugtrack.mdf;Integrated Security=True;Connect Timeout=30");
        }

        /// <summary>
        /// ***
        /// Login Submission
        /// Check if the fields are blank and if they are, produce an error message.
        /// Else set up the SQL connection and check the database for a user with matching credentials
        /// If a user is found, set Form1 variable to produce a session variable
        /// ***
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBox1.Text) || String.IsNullOrEmpty(textBox2.Text))
            {
                MessageBox.Show("Invalid Login please check username and password");
            }

            else
            {
                //mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=I:\Computing\Third_Year\ASEA\Bugtrack\Bugtrack.mdf;Integrated Security=True;Connect Timeout=30");
                mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=E:\Computing\Third_Year\ASEA\Bugtrack\Bugtrack\Bugtrack.mdf;Integrated Security=True;Connect Timeout=30");
                mySqlConnection.Open();
                // Set variables as textbox values
                string username = textBox1.Text;
                string password = textBox2.Text;
                // Check Textbox's against the database to find a user
                SqlCommand cmd = new SqlCommand("Select username, password From Users WHERE username='" + textBox1.Text + "'and password='" + textBox2.Text + "'", mySqlConnection);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    // Set session variable and close the connection
                    Form1.username = username;
                    this.Close();
                }
            }
        }

        /// <summary>
        /// Register Submission
        /// Firstly, check the fields have values which can be submitted into the database
        /// If values are present, then set up connection, and insert the values into the database
        /// after they have been bound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBox3.Text) || String.IsNullOrEmpty(textBox4.Text))
            {
                MessageBox.Show("Invalid Details");
            }

            else
            {
                using (SqlConnection connection = mySqlConnection)
                {
                    //mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=I:\Computing\Third_Year\ASEA\Bugtrack\Bugtrack.mdf;Integrated Security=True;Connect Timeout=30");
                    //mySqlConnection.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO Users (username, password) VALUES (@username, @password)");
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = connection;
                    //Bind variables from data in textbox's
                    cmd.Parameters.AddWithValue("@username", textBox3.Text);
                    cmd.Parameters.AddWithValue("@password", textBox4.Text);

                    // Open connection, Execute and close
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    // Display event message
                    MessageBox.Show("Success! Please Login");
                }
            }
        }
    }
}
