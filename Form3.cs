﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bugtrack
{
    public partial class Form3 : Form
    {
        // Initilise Variables
        private SqlConnection mySqlConnection;
        private string text;
        public Form3()
        {
            // Entry Point
            InitializeComponent();
            // Database Connection
            //mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=I:\Computing\Third_Year\ASEA\Bugtrack\Bugtrack\Bugtrack.mdf;Integrated Security=True;Connect Timeout=30");
            mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=E:\Computing\Third_Year\ASEA\Bugtrack\Bugtrack\Bugtrack.mdf;Integrated Security=True;Connect Timeout=30");

            // Call Project_list Method
            project_list();

            // Populate text labels
            label7.Text = Form1.username;
            string date = System.DateTime.Today.ToString("dd/MM/yy");
            label9.Text = date;
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            // *** //
        }

        /// <summary>
        /// Method to populate listbox with values of projects drawn from the database which haven't been fixed
        /// </summary>
        private void project_list()
        {
            // Clear Listbox and open SQL connection
            listBox1.Items.Clear();
            mySqlConnection.Open();

            // Set query and display each row in listbox
            SqlCommand cmd = new SqlCommand("SELECT project FROM Bugstore WHERE fixed = 'N'", mySqlConnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                listBox1.Items.Add(dr["project"].ToString());
            }
            // Execute query and close connection
            cmd.ExecuteNonQuery();
            mySqlConnection.Close();

        }

        /// <summary>
        /// Change the displayed information on the form when the listbox item changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Set current variable as the selected listbox object and open SQL connection
            text = listBox1.GetItemText(listBox1.SelectedItem); 
            mySqlConnection.Open();

            // Set query and populate the text fields based on the drawn values of the query
            SqlCommand select = mySqlConnection.CreateCommand();
            select.CommandType = CommandType.Text;
            select.CommandText = "Select * From Bugstore WHERE Bugstore.project='" + text + "'";
            select.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(select);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                richTextBox1.Text = (dr["code"].ToString());
                label1.Text = (dr["author"].ToString());
                label5.Text = (dr["project"].ToString());
                label11.Text = (dr["comment"].ToString());
            }
            // Close Connection
            mySqlConnection.Close();
        }

        /// <summary>
        /// Update the database values and change the status of the project to
        /// fixed.
        /// Hide the Form and return back to the main menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            SqlCommand update = mySqlConnection.CreateCommand();
            update.CommandType = CommandType.Text;
            update.CommandText = @"UPDATE Bugstore SET fixed_code=@Code, fixer=@fixer, fix_date=@date, fixed='Y' WHERE project = '" + text + "' ";
            update.Parameters.AddWithValue("@Code", richTextBox1.Text);
            update.Parameters.AddWithValue("@fixer", label7.Text);
            update.Parameters.AddWithValue("@date", label9.Text);
            mySqlConnection.Open();
            update.ExecuteNonQuery();
            mySqlConnection.Close();

            // Show status message and close form, returning to main menu
            MessageBox.Show("Code Fixed");
            this.Hide();
        }
    }
}


