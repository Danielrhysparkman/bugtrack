﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Bugtrack
{
    public partial class Form1 : Form
    {
        // Initialise Variables
        public static String username;

        public Form1()
        {
            //Entry Point
            InitializeComponent();

            // Disable buttons When username is not set
            if (username == null)
            {
                button1.Enabled = false;
                button2.Enabled = false;
                button3.Enabled = false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //***//
        }

        // Define Form 2 and show Form 2 (Upload)
        private void button1_Click(object sender, EventArgs e)
        {
            Form2 frm2 = new Form2();
            frm2.Show();
        }

        // Define Form 3 and show Form 3 (Fixing Station)
        private void button2_Click(object sender, EventArgs e)
        {
            Form3 frm3 = new Form3();
            frm3.Show();
        }

        // Define Form 4 and show Form 4 (Solutions Page)
        private void button3_Click(object sender, EventArgs e)
        {
            Form4 frm4 = new Form4();
            frm4.Show();
        }

       // Define and open Login and Register form on Link click and Set up on close method
        private void Register_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //Open Login Form
            Form5 frm5 = new Form5();
            frm5.FormClosing += new FormClosingEventHandler(ChildFormClosing);
            frm5.Show();
        }
  
        // On The Form close 
        private void ChildFormClosing(object sender, FormClosingEventArgs e)
        {
            // Set user Options and release buttons
            if (username != null)
            {
                Register.Enabled = false;
                Register.Text = "Welcome " + username;
                button1.Enabled = true;
                button2.Enabled = true;
                button3.Enabled = true;
            }
        }

        // Functionality to menu items
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }
    }
}
