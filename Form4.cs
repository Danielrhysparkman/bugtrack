﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bugtrack
{
    public partial class Form4 : Form
    {
        // Initilise Variables
        private SqlConnection mySqlConnection;
        private string text;
        public Form4()
        {
            // Entry Point
            InitializeComponent();
            
            //mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=I:\Computing\Third_Year\ASEA\Bugtrack\Bugtrack\Bugtrack.mdf;Integrated Security=True;Connect Timeout=30");
            mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=E:\Computing\Third_Year\ASEA\Bugtrack\Bugtrack\Bugtrack.mdf;Integrated Security=True;Connect Timeout=30");
           
            // Populate the listbox with values from database
            project_list();
        }


        /// <summary>
        /// Populate the listbox with values from database
        /// Open SQL connection, and build query to find bugs that have been fixed and belong to the user
        /// display all rows in the select box
        /// </summary>
        private void project_list()
        {
            // Clear the listbox and open connection
            listBox1.Items.Clear();
            mySqlConnection.Open();

            // Build query to display each row in listbox
            SqlCommand cmd = new SqlCommand("SELECT project FROM Bugstore WHERE fixed = 'Y' AND author = '" + Form1.username + "'", mySqlConnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                listBox1.Items.Add(dr["project"].ToString());
            }

            // Execute the query and close connection
            cmd.ExecuteNonQuery();
            mySqlConnection.Close();

        }


        /// <summary>
        /// Change the details shown when listbox value changed
        /// Open the connection and build a query which can search for the
        /// specific project based on the value selected from the listboz
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Initilise the selected value and open connection
            text = listBox1.GetItemText(listBox1.SelectedItem);
            mySqlConnection.Open();

            // Build query and display all information drawn from the database
            SqlCommand select = mySqlConnection.CreateCommand();
            select.CommandType = CommandType.Text;
            select.CommandText = "Select * From Bugstore WHERE Bugstore.project='" + text + "'";
            select.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(select);
            da.Fill(dt);

            // Populate the form
            foreach (DataRow dr in dt.Rows)
            {
                richTextBox1.Text = (dr["fixed_code"].ToString());
                richTextBox2.Text = (dr["code"].ToString());
                label3.Text = (dr["fixer"].ToString());
                label4.Text = (dr["fix_date"].ToString());
                label10.Text = (dr["project"].ToString());
                label11.Text = (dr["source"].ToString());
                label12.Text = (dr["class"].ToString());
                label13.Text = (dr["method"].ToString());

            }
            // Close connection
            mySqlConnection.Close();
        }

    }
}
