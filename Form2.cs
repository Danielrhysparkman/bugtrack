﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Bugtrack
{
    public partial class Form2 : Form
    {
        // Initilise Variables
        private SqlConnection mySqlConnection;

        public Form2()
        {
            // Entry Point
            InitializeComponent();
            label3.Text = Form1.username;
        }

        /// <summary>
        /// Set Database location
        /// using that connection insert bound values into the selected table
        /// Bind the variables from the form text boxes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            // Database connection
            //mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=I:\Computing\Third_Year\ASEA\Bugtrack\Bugtrack\Bugtrack.mdf;Integrated Security=True;Connect Timeout=30");
            mySqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=E:\Computing\Third_Year\ASEA\Bugtrack\Bugtrack\Bugtrack.mdf;Integrated Security=True;Connect Timeout=30");

            using (SqlConnection connection = mySqlConnection)
            {
                SqlCommand cmd = new SqlCommand("INSERT INTO Bugstore (author, method, class, source, project, code, comment, fixed) VALUES (@author, @method, @class, @source, @project, @code, @comment, 'N')");
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                // Bind Variables
                cmd.Parameters.AddWithValue("@author", label3.Text);
                cmd.Parameters.AddWithValue("@project", textBox1.Text);
                cmd.Parameters.AddWithValue("@source", textBox2.Text);
                cmd.Parameters.AddWithValue("@class", textBox3.Text);
                cmd.Parameters.AddWithValue("@method", textBox4.Text);
                cmd.Parameters.AddWithValue("@comment", textBox5.Text);
                cmd.Parameters.AddWithValue("@code", richTextBox1.Text);

                // Open connection and execute query
                connection.Open();
                cmd.ExecuteNonQuery();
            }

            // Confirmation message and hide current form
            MessageBox.Show("Code Submitted");
            this.Hide();
        }

        // Load from a file 
        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string import = File.ReadAllText(openFileDialog1.FileName);
                richTextBox1.Text = import;
            }
        }
    }
}
